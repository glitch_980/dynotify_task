from numpy.core.fromnumeric import reshape, size
from skimage.metrics import structural_similarity as ssim
import cv2
from urllib.error import URLError
import imutils
import json
from typing import Optional
from fastapi import FastAPI
from pydantic import BaseModel
from fastapi import FastAPI, Header, HTTPException
import scipy.misc
import re

app = FastAPI()
class Image(BaseModel):
    path: str


@app.get("/")
def read_root():
    return {"message": "Hello"}

@app.post("/result")
async def do_post(img1: Image, img2: Image, x_token: str = Header(...)):
    f = open("./other/config.json", 'r')
    # returns JSON object as a dictionary 
    data = json.load(f) 
    fake_secret_token = data['secret_key']
    if x_token != fake_secret_token:
        raise HTTPException(status_code=400, detail="Invalid X-Token header")

    if(re.search("^http", img1.path and img2.path) or re.search("^data", img1.path and img2.path)):
        imageA = imutils.url_to_image(img1.path)
        imageB = imutils.url_to_image(img2.path)

    else:
        imageA = cv2.imread(img1.path)
        imageB = cv2.imread(img2.path)

    if imageA.size != imageB.size:
        scale_percent = 60 # percent of original size
        width = int(imageA.shape[1] * scale_percent / 100)
        height = int(imageA.shape[0] * scale_percent / 100)
        dim = (width, height)
  
# resize both images
        imageA = cv2.resize(imageA, dim, interpolation = cv2.INTER_AREA)
        imageB = cv2.resize(imageB, dim, interpolation =  cv2.INTER_AREA)



    grayA = cv2.cvtColor(src=imageA, code=cv2.COLOR_BGR2GRAY)
    grayB = cv2.cvtColor(src=imageB, code=cv2.COLOR_BGR2GRAY)



    (score, diff) = ssim(grayA, grayB, full=True)
    diff = (diff * 255).astype("uint8")
    
    return("Similarity is: ", (score*100))
        
    


    









