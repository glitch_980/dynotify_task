# Task
#Run the server with the command uvicorn main:app --reload

#The program works as follows
# We pass three arguments, img1, img2, api-key
# You can manipulate it as much as you want

# You can use it to compare any two images from the provided samples in /images 
# Or you can upload your own test cases
#I used fastapi to build it
#I used regex so that the api can recognize image urls, i.e if they are global_urls from google,etc or local files.
#The API is authenticated with the provided API_KEY.
#The Key must be provided in the header of the POST request under the name of x-token.
#My API takes two image urls as inputs an returns the similarity ratio between them.
#Best regards.
